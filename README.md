# bugbot

Bugog_bot for skynet

Word library inside lib folder

## Build Process

Navigate into project folder, run following command:

```bash
#!/bin/bash
$ docker build -t bugbot -f Dockerfile .
```

After process ends, make sure that image is in images list:

```bash
#!/bin/bash
$ docker image ls
```

Start container:

```bash
#!/bin/bash
$ docker run --restart unless-stopped -d -e SLACK_TOKEN=<slack_app_token> -v /home/sb/go/src/bugbot/lib:/root/lib bugbot:latest
```

> `-d` flag uses to run it as the daemon (after you'll close ssh session it'll stay up)

### stop container

Run command `docker ps` to see the list of running containers: Find necessary and stop it with **CONTAINER ID** parameter:

```bash
#!/bin/bash
$ docker stop <CONTAINER ID>
```
