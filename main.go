package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"strings"

	"github.com/nlopes/slack"
	"gopkg.in/yaml.v2"
)

type libParser struct {
	Common map[string][]string `yaml:"Common"`
	Master map[string][]string `yaml:"Master"`
}

var (
	lb  *libParser
	lib = lb.getConf("lib/lib.yml")
)

func main() {
	token := os.Getenv("SLACK_TOKEN")
	api := slack.New(token)
	// user, err := api.GetUserInfo("U7RV0RXGR")
	// if err != nil {
	// 	fmt.Printf("%s\n", err)
	// 	return
	// }
	rtm := api.NewRTM()
	go rtm.ManageConnection()

Loop:
	for {
		select {
		case msg := <-rtm.IncomingEvents:
			// fmt.Print("Event Received:\n")
			switch ev := msg.Data.(type) {
			case *slack.ConnectedEvent:
				fmt.Println("\nConnection counter:", ev.ConnectionCount)

			case *slack.MessageEvent:
				// fmt.Printf("\nMessage: %v\n", ev)
				info := rtm.GetInfo()
				prefix := fmt.Sprintf("<@%s> ", info.User.ID)
				user, _ := api.GetUserInfo(ev.User)
				bugog, _ := api.GetUserInfo("U7SHPFDKN")

				if ev.User != info.User.ID && strings.HasPrefix(ev.Text, prefix) {
					directRespond(rtm, ev, prefix, bugog)
				} else {
					commonRespond(rtm, ev, user)
				}

			case *slack.RTMError:
				fmt.Printf("Error: %s\n", ev.Error())

			case *slack.InvalidAuthEvent:
				fmt.Printf("Invalid credentials")
				break Loop

			default:
				fmt.Println("Default")
				//Take no action
			}
		}

	}
}

func directRespond(rtm *slack.RTM, msg *slack.MessageEvent, prefix string, impUser *slack.User) {
	var response string
	text := msg.Text
	text = strings.TrimPrefix(text, prefix)
	text = strings.TrimSpace(text)
	text = strings.ToLower(text)

	acceptedGreetings := map[string]bool{
		"э":      true,
		"сисишь": true,
	}
	acceptedHowAreYou := map[string]bool{
		"позови бугагу": true,
		"бугагу позови": true,
	}

	if acceptedGreetings[text] {
		response = "Э!!! Cисишь нахуй!?"
		rtm.SendMessage(rtm.NewOutgoingMessage(response, msg.Channel))
	} else if acceptedHowAreYou[text] {
		response := fmt.Sprintf("Э <@%s>, посоны перетереть зовут", impUser.Name)
		rtm.SendMessage(rtm.NewOutgoingMessage(response, msg.Channel))
	}
}

func commonRespond(rtm *slack.RTM, msg *slack.MessageEvent, impUser *slack.User) {
	var response string
	// text := strings.TrimSpace(msg.Text)
	text := strings.ToLower(msg.Text)

	var category map[string][]string
	if impUser.ID == "U7SHPFDKN" {
		category = lib.Master
	} else {
		category = lib.Common
	}

	for key, val := range category {

		if (strings.Contains(text, key) && len(key) > 2) || (strings.Contains(text, key) && len(key) == len(text)) {
			response = val[rand.Intn(len(val))]
			rtm.SendMessage(rtm.NewOutgoingMessage(response, msg.Channel))
		} else {

		}
	}
}

func (lib *libParser) getConf(f string) *libParser {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	yamlFile, err := ioutil.ReadFile(dir + "/" + f)
	if err != nil {
		log.Printf("yamlFile.Get err   #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, &lib)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}
	return lib
}
