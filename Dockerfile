FROM golang:latest as builder
LABEL maintainer="default <df.rootkit@gmail.com>"
WORKDIR /go/src/bugbot
COPY . .
RUN go get gopkg.in/yaml.v2 github.com/nlopes/slack
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o bugbot .

######## Start a new stage from scratch #######
FROM alpine:latest  

WORKDIR /root/
RUN apk update && apk --no-cache add ca-certificates supervisor && \
    mkdir ./lib
# Copy the Pre-built binary file from the previous stage
COPY --from=builder /go/src/bugbot/bugbot .
COPY etc/supervisor.conf /etc/supervisord.conf
# COPY lib.yml .

# Command to run the executable
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]